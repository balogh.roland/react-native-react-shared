import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { connect } from 'react-redux';
import { addThing } from './store/modules/thing/actions';

class AppMobile extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Text>Hello from Expo App!</Text>
        <Text>From store: {this.props.thing.thing}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default connect(
  state => ({ thing: state.thing }),
  { addThing }
)(AppMobile);
