import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import store from './store';
import AppWeb from './App.web';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(
  <Provider store={store}>
    <AppWeb />
  </Provider>,
  document.getElementById('root')
);

registerServiceWorker();