export const types = {
  ADD_THING: 'ADD_THING',
};

export const addThing = (thing) => async (dispatch) => {
  dispatch({ type: types.ADD_THING, thing });
}
