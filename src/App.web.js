import React, { Component } from 'react';
import { connect } from 'react-redux';
import { addThing } from './store/modules/thing/actions';

class AppWeb extends Component {
  render() {
    return (
      <div className="AppWeb">
        <p>hello from web app</p>
        <p>From store: {this.props.thing.thing}</p>
      </div>
    );
  }
}

export default connect(
  state => ({ thing: state.thing }),
  { addThing }
)(AppWeb);
