import { combineReducers } from 'redux';

import thing from './thing/reducer';

export default combineReducers({
  thing,
});
