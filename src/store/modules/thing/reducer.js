import { types } from './actions';

const initialState = {
  thing: 'default thing',
  fetching: false,
  error: null,
};

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case types.ADD_THING:
      return {
        ...state,
        thing: action.thing,
      };
    default:
      return state;
  }
}
