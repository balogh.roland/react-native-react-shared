import { registerRootComponent } from 'expo';
import React from 'react';
import { Provider } from 'react-redux';
import store from './store';
import AppMobile from './App.mobile';

class AppMobileRoot extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <AppMobile />
      </Provider>
    );
  }
}

registerRootComponent(AppMobileRoot);
